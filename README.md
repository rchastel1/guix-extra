# guix-extra

A Guix channel for some custom packages waiting to be integrated into mainstream Guix.

## Usage 

### Adding the channel

Change your personnal channel declaration file (~/.config/guix/channels.scm) to look like this :

```scheme
(cons* 
  ; ... other custom channel declaration
  (channel
    (name 'guix-rchastel)
    (url "https://gitlab.com/rchastel1/guix-extra.git"))
  %default-channels)
```

### Update Guix
```bash
guix pull
```

### Installing packages
```bash
guix install searx-ng
```

### Reproducible build test
```bash
guix timemachine 
```
