(define-module (python-searxng)
  #:use-module ((gnu packages python-xyz) #:select (pybind11))
  #:use-module ((gnu packages python-web)
                #:select (python-socks python-httpx python-httpcore))
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system python))

(define-public python-fasttext-predict
  (package
   (name "python-fasttext-predict")
   (version "v0.9.2.2")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/searxng/fasttext-predict.git")
           (commit version)))
     (file-name (git-file-name name version))
     (sha256 (base32 "015c8ak7hxyaz3rsjjzz6zdhjiz3c4im00j21s18n9gafyp0jv06"))))
   (build-system python-build-system)
   (arguments
    (list #:configure-flags #~'("--optimize=1")))
   (propagated-inputs
    (list pybind11))
   (home-page "https://github.com/searxng/fasttext-predict")
   (synopsis " fasttext with wheels and no external dependency, but only the predict method (<1MB)")
   (description "Python package for fasttext:

    - keep only the predict method, all other features are removed

    - standalone package without external dependency (numpy is not a dependency)

    - wheels for various architectures using GitHub workflows. The script is inspired by lxml build scripts.
")
   (license license:expat)))

(define-public python-httpcore-0.17.3
  (package
   (inherit python-httpcore)
   (version "0.17.3")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/encode/httpcore")
           (commit  version)))
     (file-name (git-file-name (package-name python-httpcore) version))
     (sha256
      (base32 "11pcijc04i2h3q935qfhxpih9h01f9p0bijf2afww16daag4knv4"))))))

(define-public python-httpx-socks
  (package
   (name "python-httpx-socks")
   (version "v0.9.1")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/romis2012/httpx-socks.git")
           (commit version)))
     (file-name (git-file-name name version))
     (sha256 (base32 "0l3cn6alj6az1qd0x3a54yfx4m4j6m1lfc5zz7mbhcrd25zl7zpn"))))
   (build-system python-build-system)
   (arguments
    (list
     #:phases #~(modify-phases %standard-phases
                  (add-before 'install 'set-python-hash-seed
                    (lambda _ (setenv "PYTHONHASHSEED" "0"))))
     #:configure-flags #~'("--optimize=1" "--skip-build")))
   (propagated-inputs
    (list python-socks python-httpcore-0.17.3 python-httpx))
   (home-page "https://github.com/romis2012/httpx-socks")
   (synopsis "Proxy (HTTP, SOCKS) transports for httpx")
   (description "The httpx-socks package provides proxy transports for httpx
client. SOCKS4(a), SOCKS5(h), HTTP (tunneling) proxy supported. It uses
python-socks for core proxy functionality.")
   (license license:asl2.0)))
