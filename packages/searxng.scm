(define-module (searxng)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system python)
  )

(define-public searxng
  (package
    (name "searxng")
    (version "f5bb64c")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/searxng/searxng")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "144syq7jilf92f3pfgq61mfms32jj52sdp9lggljac1xkdi72mxg"))))
    (build-system python-build-system)
    ;; (arguments
    ;;  `(#:tests? #f ;what tests do is make online requests to each engine
    ;;    #:phases
    ;;    (modify-phases %standard-phases
    ;;      (add-after 'unpack 'relax-requirements
    ;;        (lambda _
    ;;          ;; Tests can run after build with 'searx-checker' tool in /bin.
    ;;          ;; allow using a higher dependency version
    ;;          (substitute* "requirements.txt"
    ;;            (("==") ">="))))
    ;;      (add-before 'sanity-check 'set-debug
    ;;        (lambda _
    ;;          ;; the user will need to change the secret key
    ;;          ;; https://github.com/searx/searx/issues/2278
    ;;          (setenv "SEARX_DEBUG" "1"))))))
    (propagated-inputs
     (list
      ;; python-async-timeout
      ;; python-babel
      ;; python-brotli
      ;; python-certifi
      ;; python-dateutil
      ;; python-fasttext-predict ;; absent !
      ;; python-flask
      ;; python-flask-babel
      ;; python-h2
      ;; python-httpx
      ;; python-httpx-socks ;; absent
      ;; python-jinja2
      ;; python-lxml
      ;; python-markdown-it-py
      ;; python-pygments
      ;; python-pytomlpp ;; absent !
      ;; python-pyyaml
      ;; python-redis
      ;; python-setproctitle
      ;; python-typing-extensions
      ;; python-uvloop
      ;; uwsgi
      ;; uwsgi:python
      ))
    (home-page "https://docs.searxng.org")
    (synopsis "Privacy-respecting, hackable metasearch engine.")
    (description "SearXNG is a free internet metasearch engine which aggregates results from more than 70 search services. Users are neither tracked nor profiled. Additionally, SearXNG can be used over Tor for online anonymity.")
    (license license:agpl3+)))
